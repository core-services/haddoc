<p align="center">
    <h1 align="center">Haddoc</h1>
    <p align="center">Haddoc is a LCSB flavoured documetation and landing page theme for Jekyll sites hosted in R<sup>3</sup>-GitLab. It is based on Just-the-docs theme - a modern, highly customizable, and responsive Jekyll theme for documentation with built-in search.</p>
    <p align="center"><strong><a href="https://biomap.lcsb.uni.lu">See it in action!</a></strong></p>
    <br><br><br>
</p>

![jtd](https://user-images.githubusercontent.com/896475/47384541-89053c80-d6d5-11e8-98dc-dba16e192de9.gif)

## Installation

1. Clone this repository
2. Remove origin, add new origin
3. Change the settings in `_config.yml`
4. Add and edit content
5. Git add, commit, and push

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
